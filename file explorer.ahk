
#if globalController.getContext() == "fileExplorer"
#e::
{   run explorer
    globalController.clearDisplay()
    return
}
\::
{   globalFileExplorer.setContext("back")
    globalController.clearDisplay()
    return
}
*Del::
{   if(GetKeyState("shift", "P"))
    {   globalFileExplorer.setContext("deleteAll")
        globalController.clearDisplay()
    } else
    {   globalFileExplorer.setContext("delete")
        ;will submit the input
        send {enter}
    }
    return
}
!c::
{   globalFileExplorer.closeExplorer()
    return
}
~CapsLock & f::
~f & CapsLock::
{   KeyWait, capslock
    KeyWait, f
    SetCapsLockState, off
    globalFileExplorer.toggleFileFolder()
    globalController.clearDisplay()
    return
}
#if
#e::
{   KeyWait, e, T0.5
    if(! errorlevel)
    {   KeyWait, e, DT0.5
        if(! errorlevel)
        {   run, explorer.exe
            KeyWait, LWin
            KeyWait, RWin
            return
        }
    }
    KeyWait, LWin
    KeyWait, RWin
    
    if(! globalFileExplorer)
    {    globalFileExplorer := new fileExplorer(globalController)
    }
    globalFileExplorer.start()
    return
}

class FileExplorer
{  	;an instance of the Controller class
	controller := ""
    recentFilesDir := "Addons/recentFiles.txt"
    recentFiles := ""
    startingFolders := object()
    drives := object()
    windowPid := ""
    context := false
    fileOrFolder := 2
    folderNumber := 2
    fileNumber := 0
    
    
    __new(controller)
    {   this.controller := controller
        FileRead, recentFiles, % this.recentFilesDir
        this.recentFiles := recentFiles
        
        DriveGet, drives, list, FIXED
        loop, parse, drives
        {   this.startingFolders.insert(A_LoopField ":")
            this.drives.insert(A_loopfield ":")
        }
        loop, parse, recentFiles, |
        {   this.startingFolders.insert(A_loopfield)
        }
        return this
    }
    
    start()
    {   currentDir := ""
        this.context := false
        this.controller.setContext("fileExplorer")
        folders := this.startingFolders.clone()
        this.fileOrFolder := this.folderNumber
        
        Loop
        {   selection := this.controller.getChoice(folders, "Select folder : " currentDir)
            if(this.context == "back")
            {   selection := ""
                currentDir := this.dotdot(currentDir)
            } else if(currentDir == "" && this.context == "delete")
            {   this.context := false
                this.recentFiles := RegExReplace(this.recentFiles, escapeRegex(selection "|"), "", count, 1)
                if(count)
                {   this.writeRecentFiles(this.recentFiles)
                    removeFromArray(this.startingFolders, selection)
                    folders := this.startingFolders.clone()
                }
                continue
            } else if(currentDir == "" && this.context == "deleteAll")
            {   this.context := false
                this.recentFiles := ""
                this.writeRecentFiles(this.recentFiles)
                this.startingFolders := this.drives.clone()
                folders := this.startingFolders.clone()
                continue
            } else if(this.context == "fileFolder")
            {   selection := ""
            } else if(selection == "cancelled")
            {   this.hide()
                break
            } 
            this.context := false

            currentDir := currentDir selection "\"
            StringReplace, currentDir, currentDir, \\, \, All
           
            if(currentDir == "\")
            {   currentDir := ""
                folders := this.startingFolders.clone()
               continue
            }
            if(! windowPid)
            {   if InStr(FileExist(currentDir), "D") 
                {   run, explorer.exe %currentDir%
                    WinWaitActive, ahk_class CabinetWClass, , 5
                    if Errorlevel
                    {   MsgBox, , JPGInc ERROR, ERROR waiting for windows explorer to activate  
                        this.controller.setContext("")
                        return
                    }
                    WinGet, windowPid, ID
                    WinActivate, ahk_id %windowPid%
                } else
                {   run, % currentDir
                }
            } else
            {   ControlClick, ToolbarWindow322, ahk_id %windowPid%   
                ControlSetText, Edit1, % currentDir, ahk_id %windowPid%
                ControlSend, Edit1, {enter}, ahk_id %windowPid%
            }
            if InStr(FileExist(currentDir), "D") 
            {   folders := this.getFileList(currentDir)
            } else
            {   break
            }
        }
        if(StrLen(currentDir) > 3 && ! RegExMatch(this.recentFiles, escapeRegex(currentDir "|")))
        {   this.recentFiles .= currentDir "|"
            this.startingFolders.insert(currentDir)
            this.writeRecentFiles(this.recentFiles)
        }
        this.controller.setContext("")
        return
    }
    
    toggleFileFolder()
    {   this.fileOrFolder := this.fileOrFolder == this.folderNumber ? this.fileNumber : this.folderNumber
        this.context := "fileFolder"
        return
    }
    
    getFileList(currentDir)
    {   folders := object()
        loop, % currentDir "*" , % this.fileOrFolder
        {   if A_LoopFileAttrib contains H  ; Skip any file that is either H (Hidden), R (Read-only), or S (System). Note: No spaces in "H,R,S".
            {   continue  ; Skip this file and move on to the next one.
            }
            folders.insert(A_LoopFileName)
        }
        return folders
    }
    
    setContext(context)
    {   this.context := context
        return
    }
    
    dotdot(string)
    {   return SubStr(string, 1, instr(string, "\", true, 0, 2))
    }
    
    writeRecentFiles(fileString)
    {   FileDelete, % this.recentFilesDir
        FileAppend, % fileString, % this.recentFilesDir
        return
    }
    
    closeExplorer()
    {   Loop
        {   if(winexist("ahk_class CabinetWClass"))
            {   WinClose
            } else
            {   break
            }
        }
        return
    }
    
}

